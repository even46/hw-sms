<?php

namespace hw\sms;

class HwSms
{
    /**
     * @var Config 短信配置
     */
    protected $config;

    /**
     * 构造
     * @param array $options
     * @throws \Exception
     */
    public function __construct(array $options)
    {
        if (empty($options['app_key'])) throw new \Exception('HW SMS AppKey ERROR!');
        if (empty($options['app_secret'])) throw new \Exception('HW SMS AppSecret ERROR!');
        if (empty($options['sender'])) throw new \Exception('HW SMS Sender ERROR!');
        if (empty($options['signature'])) throw new \Exception('HW SMS Signature ERROR!');
        $this->config = new Config();
        $this->config->app_key = $options['app_key'];
        $this->config->app_key = $options['app_secret'];
        $this->config->send_url = $options['send_url'] ?? 'https://rtcsms.cn-north-1.myhuaweicloud.com:10743/sms/batchSendSms/v1';
        $this->config->sender = $options['sender'];
        $this->config->signature = $options['signature'];
    }

    /**
     * send 发送短信
     * @param string $receiver // '+8615123456789,+86152****7890'
     * @param string $template_id
     * @param array $template_params
     * 选填,使用无变量模板时请赋空值 $template_params = '';
     * 单变量模板示例:模板内容为"您的验证码是${1}"时,$TEMPLATE_PARAS可填写为'["369751"]'
     * 双变量模板示例:模板内容为"您有${1}件快递请到${2}领取"时,$TEMPLATE_PARAS可填写为'["3","人民公园正门"]'
     * 模板中的每个变量都必须赋值，且取值不能为空
     * 查看更多模板和变量规范:产品介绍>模板和变量规范
     * @return string
     */
    public function send(string $receiver, string $template_id, array $template_params)
    {
        // 请求Headers
        $headers = [
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: WSSE realm="SDP",profile="UsernameToken",type="Appkey"',
            'X-WSSE: ' . $this->buildWsseHeader()
        ];
        // 请求Body
        $data = http_build_query([
            'from' => $this->config->sender,
            'to' => $receiver,
            'templateId' => $template_id,
            'templateParas' => $template_params ? json_encode($template_params, 200) : '',
            'statusCallback' => '',
            'signature' => $this->config->signature //使用国内短信通用模板时,必须填写签名名称
        ]);

        $context_options = [
            'http' => ['method' => 'POST', 'header' => $headers, 'content' => $data, 'ignore_errors' => true],
            'ssl' => ['verify_peer' => false, 'verify_peer_name' => false] //为防止因HTTPS证书认证失败造成API调用失败，需要先忽略证书信任问题
        ];

        $response = file_get_contents($this->config->send_url, false, stream_context_create($context_options));
        if ($response && ($response = json_decode($response, true)) && $response['code'] == '000000' && $response['result'] && $response['result'][0]['status'] == '000000') {
            return true;
        }
        return !empty($response['description']) ? $response['description'] : '短信发送失败';
    }

    /**
     * 构造X-WSSE参数值
     * @return string
     */
    private function buildWsseHeader(): string
    {
        date_default_timezone_set('Asia/Shanghai');
        $now = date('Y-m-d\TH:i:s\Z'); //Created
        $nonce = uniqid(); //Nonce
        $base64 = base64_encode(hash('sha256', ($nonce . $now . $this->config->app_secret))); //PasswordDigest
        return sprintf("UsernameToken Username=\"%s\",PasswordDigest=\"%s\",Nonce=\"%s\",Created=\"%s\"",
            $this->config->app_key, $base64, $nonce, $now);
    }
}