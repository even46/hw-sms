<?php

namespace hw\sms;

class Config
{
    public $send_url = ''; // 短信发送地址
    public $app_key = ''; // 短信Key
    public $app_secret = ''; // 短信密钥
    public $sender = ''; // 短信签名通道号
    public $signature = ''; //签名名称
}