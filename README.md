
# hw-sms

华为云短信发送类库

## 安装
> composer require yinxu46/hw-sms

## 使用
```injectablephp
$config = [
    'app_key' => '应用KEY',
    'app_secret' => '应用密钥',
    'sender' => 'https://rtcsms.cn-north-1.myhuaweicloud.com:10743/sms/batchSendSms/v1',
    'signature' => '短信签名',    
];
$hwSms = new HwSms($config);
$res = $hwSms->send('+8615123456789', 'template_id', ["369751"]);
if ($res !== true) {
    # 发送失败
    echo $res;
} else {
    # 发送成功
    echo "success";
}
# 发送多条短信
$res = $hwSms->send('+8615123456789,+8615123456789', 'template_id', ["369751"]);
# 发送多个参数
$res = $hwSms->send('+8615123456789,+8615123456789', 'template_id', ["参数值","参数值"]);
```